# Phabricator Bug Report

## Symptom

`arc unit` have different test results echoing behaviors based on if you use `.arcconfig` or `.arcunit`.

## Reproduce Steps

Make sure you have ran: `pip install coverage pytest`

Run `./run_test.sh` and it will automatically run two test cases. You can see the test results only reported
in the first test cases.

```
==> Test case 1: Using .arcconfig
==> Running 'arc unit': Expected to see unit test results
   FAIL  test_error.test_answer
def test_answer():
>       assert inc(3) == 5
E       assert 4 == 5
E        +  where 4 = inc(3)

test_error.py:5: AssertionError


COVERAGE REPORT
      0%     .gitignore
      0%     arcanist-pytest/.arcconfig
      0%     arcanist-pytest/__phutil_library_init__.php
      0%     arcanist-pytest/__phutil_library_map__.php
      0%     arcanist-pytest/unit/PytestConfiguredTestEngine.php
      0%     run_test.sh
    100%     test_error.py



==> Test case 2: Using .arcunit
==> Running 'arc unit': Unit test results are gone but is expected

COVERAGE REPORT
      0%     .gitignore
      0%     arcanist-pytest/.arcconfig
      0%     arcanist-pytest/__phutil_library_init__.php
      0%     arcanist-pytest/__phutil_library_map__.php
      0%     arcanist-pytest/unit/PytestConfiguredTestEngine.php
      0%     run_test.sh
    100%     test_error.py
```

## Cause

[ArcanistUnitWorkflow](https://github.com/phacility/arcanist/blob/master/src/workflow/ArcanistUnitWorkflow.php#L188)
checks that `shouldEchoTestResults` is true and render the unit test results while [ArcanistConfigurationDrivenUnitTestEngine](https://github.com/phacility/arcanist/blob/master/src/unit/engine/ArcanistConfigurationDrivenUnitTestEngine.php#L154)
checks for the opposite.
