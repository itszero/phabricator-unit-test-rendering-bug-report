#!/bin/sh

function reset() {
  rm .arcunit .arcconfig 2>/dev/null
}

echo "==> Test case 1: Using .arcconfig"
reset
cat >.arcconfig <<EOF
{
  "unit.engine": "PytestTestEngine"
}
EOF
echo "==> Running 'arc unit': Expected to see unit test results"
arc unit

echo
echo
echo
echo "==> Test case 2: Using .arcunit"
reset
cat >.arcconfig <<EOF
{
  "load": [
    "arcanist-pytest"
  ]
}
EOF
cat>.arcunit <<EOF
{
  "engines": {
    "pytest": {
      "type": "pytest",
      "include": "(\\\\.py$)"
    }
  }
}
EOF
echo "==> Running 'arc unit': Unit test results are gone but is expected"
arc unit

reset
